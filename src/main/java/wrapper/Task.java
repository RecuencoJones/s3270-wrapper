package wrapper;

import java.net.URI;
import java.util.Scanner;

/**
 * Created by David Recuenco on 11/11/14.
 */
public class Task {

    public static final String GENERAL = "general";
    public static final String SPECIFIC = "specific";

    private int id;
    private String date;
    private String description;
    private String name;
    private String type;

    public Task (int id, String date, String description, String name, String type){
        this.id = id;
        this.date = date;
        this.description = description;
        this.name = name;
        this.type = type;
    }

    public boolean isGeneral() {
         return this.type.trim().equals(GENERAL);
    }

    public String getDate() {
        return this.date;
    }

    public String getDescription() {
        return this.description;
    }

    public String getName() {
        return this.name;
    }

    public String getType() { return this.type; }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public static Task parseTask(String input){
        Scanner s = new Scanner(input);
        s.useDelimiter("\\s|\n|:");
        s.next();
        int id = s.nextInt();
        String type = s.next().trim();
        String date = s.next().trim();
        String name = s.next().trim();
        String desc = s.nextLine();
        s.close();

        return new Task(id,date,desc,name,type);
    }
}
