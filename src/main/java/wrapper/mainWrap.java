package wrapper;

/**
 * Created by David Recuenco on 28/10/2014.
 * Comentario random
 */

import gui.start;

import java.io.*;
import java.util.ArrayList;

public class mainWrap {

    //Debug var
    private static boolean DEBUG = false;

    //Process variables
    private static Process p;
    private static BufferedReader in;   //Salida estándar del programa s3270
    private static PrintWriter out;     //Entrada estándar del programa s3270

    //Task list
    public static ArrayList<Task> tasks = new ArrayList();

    public mainWrap (){

        try{

            p = null;

            // ================================
            //  Warm up
            // ================================

            /*
             * Check which OS we are runnin' on and execute s3270 process
             */
            p = checkOs();

            /*
             * These will be our communications with the s3270 process
             */
            in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            out = new PrintWriter(new OutputStreamWriter(p.getOutputStream()));

            // ================================
            //  Start procedure
            // ================================

            /*
             * Get s3270 to execute TAREAS.C
             */
            toTareas();

            toConsole("printtext string");

        }catch(IOException e){
            System.err.println("-- Couldn't create communication channels to s3270");
            e.printStackTrace();
        }catch(NullPointerException e){
            System.err.println("-- s3270 process is not recognized by the system");
            e.printStackTrace();
        }


    }

    /**
     * Method that checks the Operative System in which our application is running and fires up s3270 process.
     * @return Process s3270.
     * @throws IOException
     */
    private static Process checkOs() throws IOException{
        Process p = null;
        if(isWindows(System.getProperty("os.name").toLowerCase())){
            System.err.println("Running on Windows...");
            p = Runtime.getRuntime().exec("ws3270");
        }else if(isUnix(System.getProperty("os.name").toLowerCase())){
            System.err.println("Running on Unix...");
            p = Runtime.getRuntime().exec("s3270");
        }
        return p;
    }

    /**
     * Method that returns true if we're running on a Windows.
     * @param os "os.name" System property.
     * @return true if Windows false if a better OS.
     */
    private static boolean isWindows(String os){
        return (os.indexOf("win") >= 0);
    }

    /**
     * Method that returns true if we're running on a Unix system.
     * @param os "os.name" System property.
     * @return true if friend of Richard M. Stallman false if not.
     */
    private static boolean isUnix(String os){
        return (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0 || os.indexOf("aix") > 0 );
    }

    /**
     * Method that executes commands on s3270 until we are at the place we need.
     * @throws IOException
     */
    private static void toTareas() throws IOException{
        toConsole("connect 155.210.152.51:123",true);
        toConsole("Enter",true);
        toConsole("Enter", true);
        toConsole("printtext string", true);
        toConsole("string(\"grupo_02\")", true);
        toConsole("Tab", true);
        toConsole("string(secreto6)", true);
        toConsole("printtext string",true);
        toConsole("Enter",true);
        toConsole("Enter",true);
        toConsole("string(FLIB)",true);
        toConsole("Enter",true);
        toConsole("string(TAREAS.C)",true);
        toConsole("Enter",true);

    }

    /**
     * Method that writes a command straight to s3270 input and ignores feedback
     * @param s String with the command.
     * @throws IOException
     */
    private static void toConsole(String s){
        out.println(s);
        out.flush();

        try {
            clearInput(false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method that writes a command straight to s3270 input and shows feedback
     * @param s String with the command.
     * @param show boolean: show feedback or not
     * @throws IOException
     */
    private static void toConsole(String s, boolean show){
        out.println(s);
        out.flush();

        try {
            clearInput(show);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method that clears all junk we don't need from s3270 output.
     * @throws IOException
     */
    private static void clearInput(boolean show) throws IOException{
        String line;
    	while( (line = in.readLine()) != null && in.ready()) {
            cleanProcedure(show,line);
        }
        cleanProcedure(show,line);

    }

    /**
     * Cleaning subroutine, features:
     *      - Auto scroll page if "More..." is detected at the bottom of the page
     *      - Wait for host to finish working
     *      - Task scrapping
     * @param show inherited from clearInput()
     * @param line inherited from clearInput()
     */
    private static void cleanProcedure(boolean show, String line){
        if(line.indexOf("More...") >= 0) {
            toConsole("Enter", false);
        }
        else if(line.indexOf("Working") >= 0) {
            if(DEBUG) {
                System.out.println("WORKING");
            }
            toConsole("printtext string", true);
        }
        taskScrapping(show, line);
        if(DEBUG) {
            System.out.println(line);
        }
    }

    /**
     * Method that scraps a task from output and adds it to the task list
     * @param show inherited from clearInput()
     * @param line inherited from clearInput()
     */
    private static void taskScrapping(boolean show, String line){
        if(show && line.indexOf("TASK") == 6) {
            Task newTask = Task.parseTask(line.substring(6));
            try {
                if (tasks.get(newTask.getId()) == null) {
                    tasks.add(newTask.getId(), newTask);
                }
            }catch(Exception e){
                tasks.add(newTask.getId(), newTask);
            }
            //System.out.println(line.substring(6));
        }
    }

    /**
     * Method that lists general or specific tasks
     */
    public static void listTasks(){
        start.gui.emptyTaskList();

        toConsole("key(2)");                    //Select VIEW TASKS
        toConsole("Enter");                     //Accept
        //General tasks
        toConsole("key(1)");                    //Select GENERAL TASKS
        toConsole("Enter");                     //Accept
        toConsole("printtext string", true);    //Show screen
        //Specific tasks
        toConsole("key(2)");                    //Select SPECIFIC TASKS
        toConsole("Enter");                     //Accept
        toConsole("printtext string", true);    //Show screen

        toConsole("key(3)");                    //Back to main menu
        toConsole("Enter");                     //Accept

        toConsole("printtext string", true);

        start.gui.appendToTaskList(stringify(tasks));
    }

    /**
     * Method that adds a new GENERAL task.
     * @param date int of the date in the format DDMM.
     * @param description String giving a concise description (MAY CONTAIN SPACES).
     */
    public static void addTask(int date, String description){
        description = noSpaced(description);

        toConsole("key(1)");                    //Select ASSIGN TASK
        toConsole("Enter");                     //Accept
        toConsole("printtext string", true);
        toConsole("key(1)");                    //Select GENERAL TASK
        toConsole("Enter");                     //Accept
        toConsole("printtext string", true);
        toConsole("string("+date+")");          //Enter date
        toConsole("Enter");                     //Accept
        toConsole("printtext string", true);
        toConsole("string("+description+")");   //Enter description
        toConsole("Enter");                     //Accept
        toConsole("printtext string", true);
        toConsole("key(3)");                    //Back to main menu
        toConsole("Enter");						//Accept
        toConsole("printtext string", true);

        //System.out.println("Added GENERAL task.");

        toConsole("printtext string", true);

        listTasks();
    }

    //WIP
    /**
     * Method that adds a new SPECIFIC task.
     * @param date int of the date in the format DDMM.
     * @param name String giving the task a specific name (MAY CONTAIN SPACES)
     * @param description String giving a concise description (MAY CONTAIN SPACES)
     */
    public static void addTask(int date, String name, String description){
        name = noSpaced(name);
        description = noSpaced(description);

        toConsole("key(1)");                    //Select ASSIGN TASK
        toConsole("Enter");                     //Accept
        toConsole("printtext string", true);
        toConsole("key(2)");                    //Select SPECIFIC TASK
        toConsole("Enter");                     //Accept
        toConsole("printtext string", true);
        toConsole("string("+date+")");          //Enter date
        toConsole("Enter");                     //Accept
        toConsole("printtext string", true);
        toConsole("string("+name+")");          //Enter name
        toConsole("Enter");                     //Accept
        toConsole("printtext string", true);
        toConsole("string("+description+")");   //Enter description
        toConsole("Enter");                     //Accept
        toConsole("printtext string", true);
        toConsole("key(3)");                    //Back to main menu
        toConsole("Enter");						//Accept
        toConsole("printtext string", true);

        //System.out.println("Added SPECIFIC task.");

        toConsole("printtext string", true);

        listTasks();
    }

    /**
     * Method that removes all spaces and similar from a String.
     * @param s String with spaces.
     * @return String without spaces.
     */
    private static String noSpaced(String s){
        s.replaceAll(" ","_");
        s.replaceAll("\t","_");
        s.replaceAll("\n","_");
        s.replaceAll("\r","_");
        return s;
    }

    /**
     * Method invoked when the user wants to exit the application
     */
    public static void exit(){

        toConsole("key(3)");                    //Select EXIT
        toConsole("Enter");                     //Accept
        toConsole("Enter");                     //Back to FLIB

        /*
         * End, close and kill EVERYTHING
         */
        disconnectAndKillEverythingWithoutLeavingAnythingBehindExceptThatAnnoyinglyUnkillableJavaProcess();
    }

    /**
     * Method that ends, closes and kills every communication channel and processes related to this application except for that annoying Java process. I blame gradle.
     * @throws IOException
     */
    private static void disconnectAndKillEverythingWithoutLeavingAnythingBehindExceptThatAnnoyinglyUnkillableJavaProcess(){
        //Close session
        try {
            toConsole("Disconnect");

            //Close Process
            in.close();
            out.close();
            p.destroy();
        }catch(IOException e) {
            System.err.println("-- Couldn't close communication channels to s3270");
        }

        //End program
        System.exit(0);
    }

    /**
     * Method that formats the task list into a displayable string
     * @param tasks task list
     * @return string containing the formated list
     */
    private static String stringify(ArrayList<Task> tasks){
        String s = "";
        for(Task t : tasks){
            s = s.concat("TASK "+String.format("%5d",t.getId())+": "+t.getType()+" "+t.getDate()+" "+t.getName()+" "+t.getDescription()+"\n");
        }

        System.err.println("<===============>");
        System.err.println(s);
        return s;
    }
}
