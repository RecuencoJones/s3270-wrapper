package gui;

import javax.swing.*;
import java.awt.BorderLayout;

import wrapper.mainWrap;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class window extends JFrame{
	private JPanel panel, panel2;
	private static JTextField date;
    private static JLabel dateLabel;
	private static JTextField description;
    private static JLabel descLabel;
	private static JTextField name;
    private static JLabel nameLabel;
	private JButton submitTask;
    private JButton listTasks;
    private JTextArea taskList;
	
	public window() {
		initialize();
	}
	
	private void initialize() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        panel2 = new JPanel();
        getContentPane().add(panel2, BorderLayout.SOUTH);

        taskList = new JTextArea();
        taskList.setEditable(false);
        taskList.setText("Empty");
        taskList.setColumns(50);
        taskList.setRows(20);
        panel2.add(taskList);

		panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);


        dateLabel = new JLabel();
        dateLabel.setText("Date");
        panel.add(dateLabel);

		date = new JTextField();
		date.setToolTipText("Date");
		panel.add(date);
		date.setColumns(10);

        descLabel = new JLabel();
        descLabel.setText("Description");
        panel.add(descLabel);

        description = new JTextField();
		description.setToolTipText("Description");
		panel.add(description);
		description.setColumns(10);

        nameLabel = new JLabel();
        nameLabel.setText("Name (OPT)");
        panel.add(nameLabel);
		
		name = new JTextField();
		name.setToolTipText("Name (optional)");
		panel.add(name);
		name.setColumns(10);
		
		submitTask = new JButton("Submit!");
		submitTask.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {

                //mainWrap.listTasks();
		        int date = Integer.valueOf(start.gui.getDateText());
		        String description = start.gui.getDescriptionText();
		        String name = start.gui.getNameText();

		        //System.out.println("Adding new task [ "+date+", "+description+", "+name+" ]");
		        if (name.trim().isEmpty()) {
		            mainWrap.addTask(date, description);
		        } else {
		            mainWrap.addTask(date, name, description);
		        }
			}
		});
		panel.add(submitTask);

        listTasks = new JButton("List Tasks");
        listTasks.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                mainWrap.listTasks();

            }
        });
        panel.add(listTasks);

        this.pack();
		this.setVisible(true);
	}
	
	public String getDateText(){
		String s = date.getText();
		date.setText("");
		return s;
	}
	
	public String getDescriptionText(){
		String s = description.getText(); 
		description.setText("");
		return s;
	}

	public String getNameText(){
		String s = name.getText(); 
		name.setText("");
		return s;
	}

    public void emptyTaskList(){
        taskList.setText("");
        taskList.repaint();
    }

    public void appendToTaskList(String s){
        taskList.setText(s);
        taskList.repaint();
    }
	
}
